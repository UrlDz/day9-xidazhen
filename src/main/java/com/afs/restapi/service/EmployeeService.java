package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.apache.coyote.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.Access;
import java.util.List;

@Service
public class EmployeeService {


    private final EmployeeJPARepository employeeJPARepository;

    public EmployeeService(EmployeeJPARepository employeeJPARepository) {
        this.employeeJPARepository = employeeJPARepository;
    }

    public List<Employee> findAll() {
        return employeeJPARepository.findAll();
    }

    public EmployeeResponse findById(Long id) {
        return EmployeeMapper.toResponse(employeeJPARepository.findById(id).orElseThrow(EmployeeNotFoundException::new));
    }

    public EmployeeResponse update(Long id, EmployeeRequest employeeRequest) {
        Employee toBeUpdatedEmployee = employeeJPARepository.findById(id).get();
        Employee employee = EmployeeMapper.toEntity(employeeRequest);
        if (employee.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employee.getSalary());
        }
        if (employee.getAge() != null) {
            toBeUpdatedEmployee.setAge(employee.getAge());
        }
        Employee updateEmployee = employeeJPARepository.save(toBeUpdatedEmployee);
        return EmployeeMapper.toResponse(updateEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        return employeeJPARepository.findByGender(gender);
    }

    public Employee create(EmployeeRequest employeeRequest) {
        return employeeJPARepository.save(EmployeeMapper.toEntity(employeeRequest));
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return employeeJPARepository.findAll(pageRequest).getContent();
    }

    public void delete(Long id) {
        employeeJPARepository.deleteById(id);
    }
}
