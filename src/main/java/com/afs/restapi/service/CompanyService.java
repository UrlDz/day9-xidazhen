package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.CompanyJPARepository;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.repository.InMemoryCompanyRepository;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {

    private final EmployeeJPARepository employeeJPARepository;

    private final CompanyJPARepository companyJPARepository;

    public CompanyService(EmployeeJPARepository employeeJPARepository, CompanyJPARepository companyJPARepository) {
        this.employeeJPARepository = employeeJPARepository;
        this.companyJPARepository = companyJPARepository;
    }

    public List<Company> findAll() {
        return companyJPARepository.findAll();
    }


    public List<Company> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return companyJPARepository.findAll(pageRequest).getContent();
    }

    public CompanyResponse findById(Long id) {
        Company company = companyJPARepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        List<Employee> employees = employeeJPARepository.findByCompanyId(company.getId());
        company.setEmployees(employees);
        CompanyResponse companyResponse = CompanyMapper.toResponse(company);
        companyResponse.setEmployeesCount(company.getEmployees().size());
        return companyResponse;
    }

    public CompanyResponse update(Long id, CompanyRequest companyRequest) {
        Company updateCompany = companyJPARepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        if (companyRequest.getName() != null) {
            updateCompany.setName(companyRequest.getName());
        }
        updateCompany = companyJPARepository.save(updateCompany);
        CompanyResponse companyResponse = CompanyMapper.toResponse(updateCompany);
        companyResponse.setEmployeesCount(employeeJPARepository.findByCompanyId(id).size());
        return companyResponse;
    }

    public Company create(CompanyRequest companyRequest) {
        return companyJPARepository.save(CompanyMapper.toEntity(companyRequest));
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return employeeJPARepository.findByCompanyId(id);
    }

    public void delete(Long id) {
        companyJPARepository.deleteById(id);
    }
}
